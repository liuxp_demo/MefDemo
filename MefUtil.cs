﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefDemo
{
    class MefUtil
    {
        public void Compose()
        {
            var catalog = new DirectoryCatalog("");
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);

        }
    }
}
