﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefDemo
{
    public class PhBook : IBook
    {
        public string description { get; set ; }
        public int index { get; set; }

        public PhBook()
        {
            index = 0;
            description = "ph book";
            Console.WriteLine("create instance ph");
        }
        public void SayHi()
        {
            index++;
            Console.WriteLine(description+" "+index);
        }
    }
}
