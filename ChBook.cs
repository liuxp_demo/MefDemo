﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefDemo
{
    [Export("ch", typeof(IBook))]
    public class ChBook : IBook
    {
        public string description { get; set ; }
        public int index { get; set; }

        public ChBook()
        {
            index = 0;
            description = "chinese book";
            Console.WriteLine("create instance ch");
        }
        public void SayHi()
        {
            index++;
            Console.WriteLine(description+" "+index);
        }
    }
}
