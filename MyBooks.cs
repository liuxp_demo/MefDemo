﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefDemo
{
    [Export]
    class MyBooks
    {
        //默认单例模式
        [Import("math")]
        IBook mathBook1,mathBook2;

        //单例模式
        [Import("ch",RequiredCreationPolicy = CreationPolicy.Shared)]
        IBook chBook1,chBook2;

        //新建实例模式
        [Import("en",RequiredCreationPolicy = CreationPolicy.NonShared)]
        IBook enBook1, enBook2;

        public void SayHi()
        {
            if (mathBook1 != null)
            {
                mathBook1.SayHi();
            }
            if (mathBook2 != null)
            {
                mathBook2.SayHi();
            }

            if (chBook1 != null)
            {
                chBook1.SayHi();
            }
            if (chBook2 != null)
            {
                chBook2.SayHi();
            }

            if (enBook1 != null)
            {
                enBook1.SayHi();
            }
            if (enBook2 != null)
            {
                enBook2.SayHi();
            }
        }


    }
}
