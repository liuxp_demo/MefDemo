﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MefDemo
{
    [Export]
    public class Program
    {
        private static CompositionContainer container;

        [ImportMany]
        IEnumerable<IBook> bookList { get; set; }

        [Import]
        MyBooks myBook;

        static void Main(string[] args)
        {
            LoadApp();
            //
            Console.WriteLine("-------------------------");
            Program program = container.GetExportedValue<Program>();
            if (program != null && program.bookList != null)
            {
                foreach(IBook book in program.bookList)
                {
                    book.SayHi();
                }
            }

            //
            Console.WriteLine("-------------------------");
            IBook boo= container.GetExportedValue<IBook>("math");
            boo.SayHi();

            //
            Console.WriteLine("-------------------------");
            if (program.myBook != null)
            {
                program.myBook.SayHi();
            }

            Console.ReadLine();

        }

        static void LoadApp()
        {
            AggregateCatalog catalog = new AggregateCatalog();
            container = new CompositionContainer(catalog);
            catalog.Catalogs.Add(new DirectoryCatalog(Directory.GetCurrentDirectory()));
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
        }
    }
}
